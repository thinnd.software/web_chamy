<script type="text/javascript" src="{{asset('client/templates/default/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('client/templates/default/js/form1.js')}}"></script>
<script type="text/javascript" src="{{asset('client/templates/default/js/menu.js')}}"></script>
<script type="text/javascript" src="{{asset('client/templates/default/js/js_templates.js')}}"></script>
<script type="text/javascript" src="{{asset('client/blocks/search/assets/js/search.js')}}"></script>
<script type="text/javascript" src="{{asset('client/blocks/shopcart/assets/js/shopcart.js')}}"></script>
<script type="text/javascript" src="{{asset('client/blocks/banners/assets/js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('client/modules/home/assets/js/home.js')}}"></script>
<script type="text/javascript" src="{{asset('client/blocks/products/assets/js/default.js')}}"></script>
<script type="text/javascript" src="{{asset('client/modules/product/assets/js/home.js')}}"></script>
<script type="text/javascript" src="{{asset('client/js/main.js')}}"></script>
<script type="text/javascript" src="{{asset('client/modules/product/assets/js/lightslider.js')}}"></script>
<script type="text/javascript" src="{{asset('client/modules/product/assets/js/swiper-bundle.min.js')}}"></script>
<script type="text/javascript" src="{{asset('client/libraries/jquery/magiczoomplus/magiczoomplus.js')}}"></script>
<script type="text/javascript" src="{{asset('client/modules/product/assets/js/slick.min.js')}}"></script>
<script type="text/javascript" src="{{asset('client/modules/product/assets/js/product.js')}}"></script>

<script type="text/javascript" src="{{asset('client/modules/address/assets/js/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('client/modules/address/assets/js/cat.js')}}"></script>




    <noscript>
        <img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=126402069070999&ev=PageView&noscript=1" />
    </noscript>
    <!-- Facebook Pixel Code -->
    <script type="text/javascript">
        $(document).click(function () {
    $("#nav-right").hide();
});
$(document).ready(function() {
    $('#fs-popup-home').modal('hide');
    $(".banner-home-new").owlCarousel({
        
        autoplay: true, //Set AutoPlay to 3 seconds
        // autoplaySpeed: 1000,
        loop: true,
        autoplayTimeout: 4000,
        autoplayHoverPause: true,
        nav: false,
        dots: true,
        margin: 0,
        smartSpeed: 500,
        items: 1
    });
});
    </script>